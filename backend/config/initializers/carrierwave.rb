CarrierWave.configure do |config|

  config.cache_dir = File.join(Rails.root, 'tmp', 'uploads')

  config.fog_directory = 'cgmalandings'
  config.fog_public = true
  config.fog_credentials = {
    provider: 'AWS',
    aws_access_key_id: ENV['AWS_ACCESS_KEY'],
    aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    region: 'us-east-1',
    host: 'minio:9000',
    endpoint: 'http://localhost:9000',
    path_style: true
  }

  config.storage = :fog
end
