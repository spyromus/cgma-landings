# README

Test CGMA Landings app.

## Running

```
$ make up
```

or

```
$ docker-compose up
```

Then open: http://localhost:3000/

## Uploads

We use Minio as AWS "clone" for file uploads. The UI is at http://localhost:9001/ (u/p: minio / miniosecret ).

1. Login
2. Open http://localhost:9001/identity/account/new-account
3. Copy access and secret keys
4. Copy .env.example to .env and enter the copied keys for AWS_SECRET_KEY and AWS_SECRET_ACCESS_KEY to enable uploads

## Initial config

1. Open http://localhost:3000
2. Create admin user (any name, email, password). I used 'admin', 'admin@example.com', '12345678' for simplicity.
3. Create site 'main' with handle 'main'
4. Go to http://localhost:3000/locomotive/sites and create two more schools:
  - sevart
  - moscow

Now you have one main site and two school sites.

1. Clone https://gitlab.com/spyromus/cgma-landings-wagon repo into some other directory.
2. Run `bundle install` there
3. Update api_key in main/config/deploy.yml with the key at http://localhost:3000/locomotive/main/developers
4. Do the same for both school sites. They api keys are at school/config/deploy.yml
3. Use wagon commands to synchronize main and school sites as described at http://localhost:3000/locomotive/main/developers and in the wagon documentation.
