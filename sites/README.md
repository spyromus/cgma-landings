# README

Site configurations repository. Subdirectories contain per-site configuration.

## Dockerization

### General

This repo is dockerized to isolate dependencies (Ruby version, gems etc).

Running container:

```shell
$ docker-compose run --rm -it sites
```

### Configuration

Each school has separate Wagon tool installation that needs gems installed.

Use `bin/sites-setup <site>` to install gems for the site.

```shell
$ bin/sites-setup main
$ bin/sites-setup school
```

### Running wagon command

There are helper scripts to run wagon command inside the container:

```shell
$ bin/sites-main --help
$ bin/sites-school --help
```

### Serving

```
$ bin/sites-main serve -h 0.0.0.0
$ bin/sites-school serve -h 0.0.0.0
```

Then open: http://localhost:3333
