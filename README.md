# CGMA Landings

CGMA Landings project repository.

- `backend` is the Rails backend CMS app
- `sites` holds Locomotive CMS Wagon template definitions

## Operations

### Launch backend

```shell
$ docker-compose up backend
```

or

```shell
$ bin/backend
```

### Enter sites container

```shell
$ docker-compose run --rm sites
```

### Sites-specific commands

These are currently in the `sites/bin` directory:

- `sites/bin/setup <sitename>` initializes libraries for the site
- `sites/bin/<sitename> <wagon commands>` runs wagon commands for the given site (you may need to create more of these or pass sitename as an argument, which is better)
